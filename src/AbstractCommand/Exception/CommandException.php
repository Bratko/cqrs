<?php

namespace Bratko\Cqrs\AbstractCommand\Exception;

class CommandException extends \Exception
{
}
