<?php

namespace Bratko\Cqrs\AbstractCommand\Exception;

class CommandTypeException extends CommandException
{
}
