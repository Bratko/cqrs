<?php

namespace Bratko\Cqrs\AbstractCommand\Exception;

class CommandTooManyCallsException extends CommandException
{
}
