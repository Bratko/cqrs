<?php



namespace  Bratko\Cqrs\AbstractCommand\Exception;

use Exception;

class CommandArgValidationException extends Exception
{
}
