<?php

namespace Bratko\Cqrs\AbstractCommand;

interface CommandValidatorInterface
{
    public function validate($command);
}
