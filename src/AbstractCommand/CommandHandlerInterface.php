<?php

namespace Bratko\Cqrs\AbstractCommand;

interface CommandHandlerInterface
{
    /**
     * @param $command
     * @param null $transaction
     *
     * @return mixed
     *
     * @throws \Bratko\Cqrs\AbstractCommand\Exception\CommandValidationException
     */
    public function handle($command, $transaction = null);
}
