<?php

namespace Bratko\Cqrs\AbstractCommand;

interface CommandArgValidator
{
    /**
     * @param $value
     * @param array $additionalArgs
     *
     * @return mixed
     */
    public function validate($value, ...$additionalArgs);
}
