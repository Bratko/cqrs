<?php

namespace Bratko\Cqrs\AbstractCommand;

use Bratko\Cqrs\AbstractCommand\Exception\CommandValidationException;

abstract class AbstractCommandHandler implements CommandHandlerInterface
{
    /**
     * @var \Bratko\Cqrs\AbstractCommand\AbstractCommandValidator
     */
    private $validator;

    public function __construct(
        AbstractCommandValidator $validator = null
    ) {
        $this->validator = $validator;
    }

    /**
     * @return CommandValidatorInterface|null
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * @param object $command
     * @param null   $transaction
     *
     * @return mixed
     *
     * @throws CommandValidationException
     * @throws Exception\CommandExecutionException
     * @throws Exception\CommandTypeException
     */
    public function handle($command, $transaction = null)
    {
        $this->validate($command);

        return $this->execute($command);
    }

    /**
     * @return bool
     */
    public function hasValidator()
    {
        return (bool) $this->validator;
    }

    /**
     * @param object $command
     *
     * @throws CommandValidationException
     * @throws Exception\CommandTypeException
     */
    protected function validate($command)
    {
        if (null === $this->validator) {
            return;
        }

        $validationResult = $this->validator->validate($command);
        if ($validationResult->hasErrors()) {
            throw new CommandValidationException($validationResult);
        }
    }

    /**
     * @param $command
     *
     * @return mixed
     *
     * @throws \Bratko\Cqrs\AbstractCommand\Exception\CommandExecutionException
     */
    abstract protected function execute($command);
}
